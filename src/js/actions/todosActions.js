import axios from "axios";

/**
 * @namespace todosActions
 */

/**
 * fetchTodos - description
 *
 * @return {type}  description
 * @memberof todosActions
 */
export function fetchTodos(){
  return function(dispatch){
    axios.get("todos.json")
    .then((response) => {
      dispatch({type: "FETCH_TODOS_FULFILLED", payload: response.data})
    })
    .catch((err) => {
      dispatch({type: "FETCH_TODOS_REJECTED", payload: err})
    })
  }
}

/**
 * addTodo - add todo
 *
 * @param  {type} id   description
 * @param  {type} text description
 * @return {type}      description
 * @memberof todosActions
 */
export function addTodo(id, text){
  return{
    type: 'ADD_TODO',
    payload: {
      id,
      text,
    },
  }
}

/**
 * updateTodo - edit todo
 *
 * @param  {type} id   description
 * @param  {type} text description
 * @return {type}      description
 * @memberof todosActions
 */
export function updateTodo(id, text) {
  return {
    type: 'UPDATE_TODO',
    payload: {
      id,
      text,
    }
  }
}

/**
 * tickTodo - tick toggle todo
 *
 * @param  {type} id description
 * @return {type}    description
 * @memberof todosActions
 */
export function tickTodo(id) {
  return {
    type: 'TICK_TODO',
    payload: {
      id,
    }
  }
}

/**
 * deleteTodo - delete todo
 *
 * @param  {type} id description
 * @return {type}    description
 * @memberof todosActions
 */
export function deleteTodo(id){
  return {type: 'DELETE_TODO', payload: id}
}

/**
 * deleteDoneTodos - delete the todos which are done
 *
 * @return {type}  description
 * @memberof todosActions
 */
export function deleteDoneTodos(){
  return {type: 'DELETE_DONE_TODO'}
}

/**
 * editModeTodo - go into edit mode of todo
 *
 * @param  {type} id description
 * @return {type}    description
 * @memberof todosActions
 */
export function editModeTodo(id) {
  return {
    type: 'EDIT_MODE_TODO',
    payload: {
      id,
    }
  }
}

/**
 * tickAllTodos - tick off all todos
 *
 * @return {type}  description
 * @memberof todosActions
 */
export function tickAllTodos(){
  return {type: 'TICK_ALL_TODO'}
}
