import React from "react";
import {connect} from "react-redux"
import $ from "jquery"

import {updateTodo, editModeTodo} from "../actions/todosActions"

/**
*
*@class Edit
*/
@connect((store) => {
  return{
    todos: store.todos.todos,
  };
})
export default class Edit extends React.Component {

  /**
   * updateTodo - handler edit todo
   * @param  {type} id description
   * @param  {type} e  description
   * @return {type}    description
   */
  updateTodo(id, e){
    if(e.key === "Enter"){
      this.props.dispatch(updateTodo(id, e.target.value));
    }
  }

  /**
   * editMode - handler to edit mode
   *
   * @param  {type} id description
   * @return {type}    description
   */
  editMode(id){
    this.props.dispatch(editModeTodo(id));
  }

    render() {
      if(this.props.todo.edit){
        return <textarea rows="2" cols="68" defaultValue={this.props.todo.text}
                      onKeyPress={this.updateTodo.bind(this, this.props.todo.id)}
                      onBlur={this.editMode.bind(this, this.props.todo.id)}  autoFocus />;
      }else{
        return <span onDoubleClick={this.editMode.bind(this, this.props.todo.id)}>{this.props.todo.text}</span>;
      }
    }
}
