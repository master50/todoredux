import React from "react"
import {connect} from "react-redux"
import $ from "jquery"

import Edit from"./Edit";

import {fetchTodos, deleteTodo, tickTodo, deleteDoneTodos, addTodo, tickAllTodos} from "../actions/todosActions"

/**
 * @class Layout
 */
 @connect((store) => {
    return {
        todos: store.todos.todos,
    };
})
export default class Layout extends React.Component {

    /**
     * componentWillMount - will run when page loads and fetch todos
     * @return {type}  description
     */
    componentWillMount() {
        this.props.dispatch(fetchTodos());
    }

    /**
     * tickAllTodos - handler to tick off all todos
     *
     * @return {type}  description
     */
    tickAllTodos() {
        this.props.dispatch(tickAllTodos());
    }

    /**
     * deleteTodo - handler delete todo
     *
     * @param  {type} id description
     * @return {type}    description
     */
    deleteTodo(id) {
        if (confirm("Sure you want to delete the todo?")) {
            this.props.dispatch(deleteTodo(id));
        }
    }

    /**
     * deleteDoneTodos - handler delete completed todos
     *
     * @return {type}  description
     */
    deleteDoneTodos() {
        if (confirm("Sure you want to delete alle completed todos?")) {
            this.props.dispatch(deleteDoneTodos());
        }
    }

    /**
     * tickTodo - handler tick todo
     *
     * @param  {type} id description
     * @return {type}    description
     */
    tickTodo(id) {
        this.props.dispatch(tickTodo(id));
    }

    /**
     * addTodo - description
     *
     * @param  {type} e description
     * @return {type}   description
     */
    addTodo(e) {
        if (e.key === "Enter") {
            this.props.dispatch(addTodo(Date.now(), e.target.value));
            e.target.value = "";
        }
    }

    /**
     * isAnyTodoCompleted - gets boolean if any todo are completed or not
     *
     * @return {type}  description
     */
    isAnyTodoCompleted() {
        const {todos} = this.props;
        return Boolean(todos.filter(todo => todo.done === true).length);
    }

    render() {
        const {todos} = this.props;
        const anyTodoCompleted = this.isAnyTodoCompleted();

        if (!todos) {
            return <span>loading data...</span>
        }

        var that = this;
        return (
            <table class="table table-hover">
                <thead>
                <tr>
                    <th><input type="checkbox" onChange={this.tickAllTodos.bind(this)} class="strikethrough"
                               checked={anyTodoCompleted}/><span></span>Todo
                    </th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                {todos.map(function (todo) {
                    return (
                        <tr key={todo.id}>
                            <td>
                                <input type="checkbox" onChange={that.tickTodo.bind(that, todo.id)}
                                       class="strikethrough" checked={todo.done}/>
                                <Edit todo={todo}/>
                            </td>
                            <td>
                                <button class="btn btn-danger" onClick={that.deleteTodo.bind(that, todo.id)}>Delete
                                </button>
                            </td>
                        </tr>
                    );
                })}
                <tr>
                    <td>
                        <input type="text" placeholder="Add Todo - Confirm with Enter" size="68"
                               onKeyPress={this.addTodo.bind(this)}/>
                    </td>
                    <td>
                        <button class="btn btn-warning" disabled={!anyTodoCompleted}
                                onClick={this.deleteDoneTodos.bind(this)}>Delete Completed
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        )
    }
}
