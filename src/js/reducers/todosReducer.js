export default function reducer(state={
  todos: [],
  fetching: false,
  fetched: false,
  error: null
}, action){
  switch(action.type){
    case "FETCH_TODOS": {
      return {...state, fetching: true}
    }
    case "FETCH_TODOS_REJECTED": {
      return {...state, fetching: false, error: action.payload}
    }
    case "FETCH_TODOS_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        todos: action.payload,
      }
    }
    case "ADD_TODO": {
      return{
        ...state,
        todos: [...state.todos, action.payload],
      }
    }
    case "UPDATE_TODO": {
      const {id, text} = action.payload
      const newTodos = [...state.todos]
      const todoToUpdate = newTodos.findIndex(todo => todo.id === id)
      newTodos[todoToUpdate].text = text
      newTodos[todoToUpdate].edit = false;

      return {
        state,
        todos: newTodos,
      }
    }
    case "TICK_TODO": {
      const {id} = action.payload
      const newTodos = [...state.todos]
      const todoToUpdate = newTodos.findIndex(todo => todo.id === id)
      newTodos[todoToUpdate].done = !newTodos[todoToUpdate].done;

      return {
        state,
        todos: newTodos,
      }
    }
    case "DELETE_TODO": {
      return{
        ...state,
        todos: state.todos.filter(todo => todo.id !== action.payload),
      }
    }
    case "DELETE_DONE_TODO": {
      return{
        ...state,
        todos: state.todos.filter(todo => todo.done !== true),
      }
    }
    case "EDIT_MODE_TODO": {
      const {id} = action.payload
      const newTodos = [...state.todos]
      const todoToUpdate = newTodos.findIndex(todo => todo.id === id)
      newTodos[todoToUpdate].edit = !newTodos[todoToUpdate].edit;

      return {
        state,
        todos: newTodos,
      }
    }
    case "TICK_ALL_TODO": {
      const newTodos = [...state.todos]
      const anyCompleted = Boolean(newTodos.filter(todo => todo.done === true).length)
      newTodos.forEach(function(todo) {
        todo.done = !anyCompleted;
      });

      return{
        ...state,
        todos: newTodos,
      }
    }
  }

  return state
}
